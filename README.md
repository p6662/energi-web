# Energi web

A backend application to help select the best DevOps candidates all over the world.

# Code of Conduct

We are a distributed commnunity, to provide a welcoming environment to people of all background and culture, please read and adhere to our [code of conduct](CODE_OF_CONDUCT.md).

# Contributing

Please refer to [contributing guide](CONTRIBUTING.md).

# License

This code is licensed under the [Apache-2.0](https://www.apache.org/licenses/LICENSE-2.0).

# Code

To develop locally you need to:

    git clone git@gitlab.com:p6662/energi-web.git && cd energi-web
    npm i
    npm start










