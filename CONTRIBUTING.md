# Discuss

Before you start working on a new issue, make sure you've discussed it with the team. That will both give you more context and certainty on what needs to be done and also prevent you from working on stale tickets, doing repeated work or working on something that is currently being discussed.

# Feature/Bugfix
To work on a new feature/bugfix create a branch following the standard $type/$ticket
    
    git switch -c $type/$ticket

So, if you're working on a bugfix for a ticket called DEVOPS-123

    git switch -c bugfix/DEVOPS-123

The CI will test your code and build the container images, once you're happy with your work, create a pull request to the main branch and choose the appropriate code reviewers. 

Discuss all the relevant proposed changes before working on the fixes.

Once that's done, merge your code to the main branch and test it on the staging environment.


# Production

Depending on the release cycle, create a tag with the chosen version and push it to gitlab. 
The CI will again test the codebase and will deploy to production.

