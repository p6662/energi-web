FROM node:16

WORKDIR /app
COPY . .
RUN chown -R 1000:1000 /app

USER 1000
RUN npm i

CMD node index.js
